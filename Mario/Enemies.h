#ifndef ENEMIES_H
#define ENEMIES_H

#include "GameEngine.h"

class CMario;
class CBlocks;

class CEnemy : public CGameObject
{
public:
	virtual void kickFromTop(CMario* mario) = 0;
	virtual void kickFromBottom(CMario* mario) = 0;
	virtual bool isAlive() const = 0;
	virtual void touchSide(CMario* mario) = 0;
    virtual bool isInBulletState() const {return false;}
	virtual void fired(CMario* mario);
	void update(int delta_time);
protected:
	virtual void start() override;
	CMario* mario();
	void addScoreToPlayer(int score);
	void checkNextTileUnderFoots();
	void checkCollideOtherCharasters();
	void checkFallUndergound();
	void updateCollision(float delta_time);
	void updatePhysics(float delta_time, float gravity);
	ECollisionTag m_collision_tag;
	const float gravity_force = 0.0015f;
	Vector m_speed;
	Vector m_direction = Vector::left;
	CMario* m_mario = nullptr;
	CBlocks* m_blocks = nullptr;
	const float m_run_speed = -0.05f;
};

class CSpinny : public CEnemy
{
public:
	CSpinny(const Vector& position, const Vector& speed, const Vector& walk_direction);
	virtual void draw(sf::RenderWindow* render_window) override;
	virtual void update(int delta_time) override;
	virtual void kickFromTop(CMario* mario) override;
	virtual void kickFromBottom(CMario* mario) override;
	virtual void touchSide(CMario* mario) override;
	virtual bool isAlive() const override;
private:
	enum State { Egg, Normal, Died } m_state = Egg;
	Vector m_walk_direction;
	void setState(State state);
	Animator m_animator;
};

class CLakity : public CEnemy
{
public:
	CLakity();
	virtual void draw(sf::RenderWindow* render_window) override;
	virtual void update(int delta_time) override;
	virtual void kickFromTop(CMario* mario) override;
	virtual void kickFromBottom(CMario* mario) override;
	virtual void touchSide(CMario* mario) override;
	virtual bool isAlive() const override;
	void runAway(const Vector& run_direction);
	void start() override;
private:
	const int fire_rate = 2500;
	float m_fire_timer = 0;
	float m_died_timer = 0;
	enum State { Normal, Died, RunAway } m_state = Normal;
	void setState(State state);
	Animator m_animator;
};

class CLakitySpawner : public CGameObject
{
public:
	CLakitySpawner();
	void update(int delta_time) override;
protected:
	void onActivated() override;
	void start() override;
private:
	const int check_interval = 5000;
	CLakity* m_lakity = NULL;
	CMario* m_mario = NULL;
	float m_lakity_checker_timer = 0;
};

#endif
