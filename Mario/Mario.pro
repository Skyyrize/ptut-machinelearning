TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
    main.cpp \
    Blocks.cpp \
    Enemies.cpp \
    GameEngine.cpp \
    Geometry.cpp \
    Items.cpp \
    Mario.cpp \
    SuperMarioGame.cpp \
    tinyxml2.cpp

HEADERS += \
    Blocks.h \
    Enemies.h \
    GameEngine.h \
    Geometry.h \
    Items.h \
    Mario.h \
    SuperMarioGame.h \
    TileMap.h \
    tinyxml2.h


win32: LIBS += -L$$PWD/../../../../Qt/Tools/SFML/lib/ -lsfml-graphics -lsfml-window -lsfml-system -lsfml-audio

INCLUDEPATH += $$PWD/../../../../Qt/Tools/SFML/include
DEPENDPATH += $$PWD/../../../../Qt/Tools/SFML/include
