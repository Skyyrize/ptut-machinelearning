#include "Enemies.h"
#include "Mario.h"
#include "Blocks.h"
#include "SuperMarioGame.h"
#include <cmath>

void CEnemy::fired(CMario* mario)
{
    kickFromBottom(mario);
}

void CEnemy::checkNextTileUnderFoots()
{
    if (m_speed.y == 0)
    {
        Vector own_center = getBounds().center();
        Vector opposite_vector = math::sign(m_speed.x)*Vector::right;
        bool is_next_under_foot = m_blocks->isCollidableBlock(m_blocks->toBlockCoordinates(own_center + 20*opposite_vector + 32*Vector::down));
        bool is_prev_under_foot = m_blocks->isCollidableBlock(m_blocks->toBlockCoordinates(own_center - 60*opposite_vector + 32*Vector::down));
        bool is_prev_back = m_blocks->isCollidableBlock(m_blocks->toBlockCoordinates(own_center - 50*opposite_vector));
        bool is_next_back = m_blocks->isCollidableBlock(m_blocks->toBlockCoordinates(own_center + 50*opposite_vector));

        if ((!is_next_under_foot && !is_prev_back) && (is_next_under_foot || is_prev_under_foot))
            m_speed.x = -m_speed.x;
    }
}

void CEnemy::checkCollideOtherCharasters()
{
    auto enemies = getParent()->findObjectsByType<CEnemy>();
    for (auto enemy : enemies)
        if (enemy != this && enemy->isAlive() && enemy->getBounds().isIntersect(getBounds()))
        {
            bool is_enemy_in_bullet_state_also = enemy->isInBulletState();
            enemy->kickFromBottom(nullptr);
            if (is_enemy_in_bullet_state_also)
            {
                kickFromBottom(nullptr);
                break;
            }
        }
}

void CEnemy::updateCollision(float delta_time)
{
    m_collision_tag = ECollisionTag::none;
    setPosition(m_blocks->collsionResponse(getBounds(), m_speed, delta_time, m_collision_tag));
}

void CEnemy::updatePhysics(float delta_time, float gravity)
{
    m_speed += Vector::down*gravity * delta_time;
    move(delta_time*m_speed);
}

void CEnemy::checkFallUndergound()
{
    if (getPosition().y > 1000)
        getParent()->removeObject(this);
}

CMario* CEnemy::mario()
{
    return m_mario;
}

void CEnemy::addScoreToPlayer(int score)
{
    MarioGame().addScore(score, getBounds().center());
}

void CEnemy::start()
{
    m_mario = getParent()->findObjectByName<CMario>("Mario");
    m_blocks = getParent()->findObjectByName<CBlocks>("Blocks");
    assert(m_mario && m_blocks);
}

void CEnemy::update(int delta_time)
{
    checkFallUndergound();
}


CSpinny::CSpinny(const Vector& position, const Vector& speed, const Vector& walk_direction)
{
    setPosition(position);
    m_speed = speed;
    m_walk_direction = walk_direction;
    setSize({ 31, 32 });

    const sf::Texture& texture = *MarioGame().textureManager().get("Enemies");
    m_animator.create("walk", texture, Vector(64, 80), Vector(32, 32), 2, 1, 0.005f);
    m_animator.create("egg", texture, Vector(128, 80), Vector(32, 32), 2, 1, 0.005f);
    m_animator.create("died", texture, { 64,80 + 32,32,-32 });

    setState(State::Egg);
}

void CSpinny::draw(sf::RenderWindow* render_window)
{
    m_animator.setPosition(getPosition());
    m_animator.draw(render_window);
}

void CSpinny::setState(State state)
{
    m_state = state;

    if (m_state == State::Normal)
    {
        m_animator.play("walk");
        m_speed.x = m_run_speed;
        if (m_walk_direction == Vector::right)
            m_speed = -m_speed;
    }
    else if (m_state == State::Died)
    {
        m_animator.play("died");
        addScoreToPlayer(400);
        MarioGame().playSound("kick");
    }
    else if (m_state == State::Egg)
    {
        m_animator.play("egg");
    }
}

void CSpinny::update(int delta_time)
{
    CEnemy::update(delta_time);
    m_animator.update(delta_time);

    switch (m_state)
    {
    case (State::Egg):
    {
        updatePhysics(delta_time, gravity_force / 2);
        updateCollision(delta_time);
        if ((m_collision_tag & ECollisionTag::left) || (m_collision_tag & ECollisionTag::right))
            m_speed.x = -m_speed.x;
        if ((m_collision_tag & ECollisionTag::floor) || (m_collision_tag & ECollisionTag::cell))
        {
            m_speed.y = 0;
            setState(State::Normal);
        }
        break;
    }
    case (State::Normal):
    {
        updatePhysics(delta_time, gravity_force / 2);
        updateCollision(delta_time);
        if ((m_collision_tag & ECollisionTag::left) || (m_collision_tag & ECollisionTag::right))
            m_speed.x = -m_speed.x;
        if ((m_collision_tag & ECollisionTag::floor) || (m_collision_tag & ECollisionTag::cell))
            m_speed.y = 0;
        m_animator.flipX(m_speed.x > 0);
        break;
    }
    case (State::Died):
    {
        updatePhysics(delta_time, gravity_force / 2);
        break;
    }
    }

}

void CSpinny::kickFromTop(CMario* mario)
{
    mario->reciveDamage();
}

void CSpinny::kickFromBottom(CMario* mario)
{
    setState(State::Died);
}

void CSpinny::touchSide(CMario* mario)
{
    mario->reciveDamage();
}

bool CSpinny::isAlive() const
{
    return m_state != State::Died;
}

//---------------------------------------------------------------------------------------------------------------

CLakity::CLakity()
{
    setName("Lakity");
    setSize({ 32, 48 });
    const sf::Texture& texture = *MarioGame().textureManager().get("Enemies");
    m_animator.create("fire", texture, { 0,128,32,48 });
    m_animator.create("fly", texture, { 32,128,32,48 });
    m_animator.create("died", texture, { 32,128 + 48,32,-48 });
    setState(State::Normal);
}

void CLakity::draw(sf::RenderWindow* render_window)
{
    m_animator.setPosition(getPosition());
    m_animator.draw(render_window);
}

void CLakity::update(int delta_time)
{
    CEnemy::update(delta_time);

    switch (m_state)
    {
    case (State::Normal):
    {
        //move porcessing
        float diff_x = m_mario->getPosition().x - getPosition().x;
        m_speed.x += math::sign(diff_x)*sqrt(std::abs(diff_x)) / 4000;
        m_speed.x = math::clamp(m_speed.x, -0.35f, 0.35f);

        move(m_speed*delta_time);

        //fire processing
        m_fire_timer += delta_time;
        if (m_fire_timer > fire_rate)
        {
            Vector fly_direction = (m_mario->getPosition().x > getPosition().x) ? Vector::right : Vector::left;
            CSpinny* spinny = new CSpinny(getPosition() - Vector(0, 10), Vector(-0.05f*fly_direction.x, -0.2f), fly_direction);
            getParent()->addObject(spinny);
            m_fire_timer = 0;
            m_animator.play("fly");
        }
        if (m_fire_timer > fire_rate*0.8f)
            m_animator.play("fire");

        break;
    }
    case (State::RunAway):
    {
        move(m_speed*delta_time);
        m_died_timer += delta_time;
        if (m_died_timer > 2000)
            getParent()->removeObject(this);
        break;
    }
    case (State::Died):
    {
        updatePhysics(delta_time, gravity_force / 2);
        break;
    }
    }
}

void CLakity::kickFromTop(CMario* mario)
{
    setState(State::Died);
}

void CLakity::kickFromBottom(CMario* mario)
{
    setState(State::Died);
}

void CLakity::touchSide(CMario* mario)
{
    mario->reciveDamage();
}

bool CLakity::isAlive() const
{
    return m_state != State::Died;
}

void CLakity::setState(State state)
{
    m_state = state;
    if (m_state == State::Died)
    {
        m_animator.play("died");
        m_speed = Vector::zero;
        addScoreToPlayer(1200);
        MarioGame().playSound("kick");
    }
    else if (m_state == State::Normal)
        m_animator.play("fly");
}

void CLakity::runAway(const Vector& run_direction)
{
    m_speed.x = run_direction.x * 0.2f;
    setState(State::RunAway);
}

void CLakity::start()
{
    CEnemy::start();
}

//----------------------------------------------------------------------------------------------------------------

CLakitySpawner::CLakitySpawner()
{
}

void CLakitySpawner::update(int delta_time)
{
    CGameObject::update(delta_time);

    m_lakity_checker_timer += delta_time;
    if (m_lakity_checker_timer > check_interval)
    {
        m_lakity = getParent()->findObjectByName<CLakity>("Lakity");
        Rect camera_rect = getParent()->castTo<CMarioGameScene>()->cameraRect();


        if (!m_lakity)  	// Add Lakity to the scene processing
        {
            if (getBounds().isContainByX(m_mario->getPosition()))
            {
                m_lakity = new CLakity();
                m_lakity->setPosition(camera_rect.left() - 32, 64);
                getParent()->addObject(m_lakity);
            }
        }
        m_lakity_checker_timer = 0;
    }

    if (m_lakity)              // Throw Lakity from the scene processing
    {
        Rect camera_rect = getParent()->castTo<CMarioGameScene>()->cameraRect();
        if (m_lakity->getPosition().x > getBounds().right() + camera_rect.size().x / 2)
        {
            m_lakity->runAway(Vector::left);
            m_lakity = NULL;
        }
        else if (m_lakity->getPosition().x < getBounds().left() - camera_rect.size().x / 2)
        {
            m_lakity->runAway(Vector::right);
            m_lakity = NULL;
        }
    }

}

void CLakitySpawner::onActivated()
{
    setSize({ getProperty("width").asFloat(), getProperty("height").asFloat() });
}

void CLakitySpawner::start()
{
    m_mario = getParent()->findObjectByName<CMario>("Mario");
}
